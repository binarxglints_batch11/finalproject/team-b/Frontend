import React, {useEffect, useState} from 'react';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import EmptyOrder from './EmptyOrder';
import CardOrder from './CardOrder';
import axios from 'axios'
import authHeader from "../../services/auth-header";
import UserService from "../../services/user.service";
import Scroll from '../searchpage/Scroll'

import Scroll2 from './Scroll2'
import './OrderDetail.css'
import qs from 'qs'

const OrderDetail =()=>{
    const API_URL = "https://travelook.gabatch11.my.id/reservation/user?page=1"
    const [listOrder, setListOrder] = useState([])

    const data = qs.stringify({ 
        status: "active",
        
       });


    useEffect(() => {

        axios.post(API_URL, data, {headers: authHeader()} ) 
    .then(response => {
            setListOrder(response.data.data)
      })  
        .catch((error) => {
            
            console.log(error. response. data)
            console.log(error. response. status);
         
        });
    
      }, []);


    return(
        <div>

            <div className="cont-order-detail">
                <div className="only-margin">
                    <h4>Orders</h4>
                    <p>After book a stay you can manage orders here </p>
                </div>
                
                <div>
                    <ul className="ul-order">
                        <li>All Request</li>
                        <li>Success</li>
                        <li>On process</li>
                        <li>Failed</li>
                    </ul>
                </div>

            </div>

            <div className="cont-order-boxcard">
                {
                    listOrder.length > 0 
                    // ? <CardOrder listOrder={listOrder}/>
                    ? <Scroll2><CardOrder listOrder={listOrder}/></Scroll2>
                    : <EmptyOrder/>
                }

            </div>

        </div>
        
    )
}

export default OrderDetail