import React, {useEffect, useState} from 'react';
import HeadNav from './HeadNav';
import HeadNavAfterLogin from './HeadNavAfterLogin';
import JumboHomepage from './JumboHomepage';
import Content from './Content';
import Footer from './Footer';
import ErrorBoundry from '../network-err-page/ErrorBoundry';
import { useMediaQuery } from 'react-responsive'
import UserService from "../../services/user.service";


import OnOffModal from '../store/OnOffModal';

// HeadNav dan HeadNavAfterLogin saat ini gantia-gantian jika mau testing soal belum disambung

const Homepage = () => {

    // const [getOneUserDatax, setOneUserDatax] = useState("")

    // useEffect(() => {
    //     UserService.getOneUserData().then(
    //       (response) => {
    //         setOneUserDatax(response.data);
    //       },
    //       (error) => {
    //         const _content =
    //           (error.response &&
    //             error.response.data &&
    //             error.response.data.message) ||
    //           error.message ||
    //           error.toString();
    
    //           setOneUserDatax(_content);
    //       }
          
    //     );

    //   }, []);

      // console.log(getOneUserDatax.data.first_name);

      // let firstNameOnHeadNav = getOneUserDatax.data.first_name
    // useEffect(() => {
    //        var axios = require('axios');

    //           var config = {
    //             method: 'get',
    //             url: 'https://travelook.gabatch11.my.id/user/',
    //             headers: { Authorization: 'Bearer ' + localStorage.getItem('token') }
    //           };

    //           axios(config)
    //           .then(function (response) {
    //             console.log(response.data.token);
    //           })
    //           .catch(function (error) {
    //             console.log(error);
    //           });
    //   }, []);

    // const {isSignIn, ClickSignInChangeHeadNav} = OnOffModal();

    return(
        <div>
            {/* {
                isSignIn ? <HeadNavAfterLogin/> : <HeadNav/>
                // isSignIn ? <HeadNav/> : <HeadNavAfterLogin/>

            } */}

            {/* <HeadNav/> */}
            {/* <ErrorBoundry> */}
                <HeadNavAfterLogin/>


                <JumboHomepage/>
                <Content/>
                <Footer/>
            {/* </ErrorBoundry>     */}
        </div> 
    )
}

export default Homepage